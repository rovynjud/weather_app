package com.example.marou.projecte;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    /*EditText etCity;
    ImageView ivWeather;
    TextView tvCoords;
    Button btnRefresh;
    TextView tvWeather;*/

    ArrayList<String> items = null;
    ArrayAdapter<Weather> adapter;
    WeatherViewModel model;
    Fragment fragment;


    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        String[] data = {"http://virtual.ecaib.org/theme/image.php/adaptable/attendance/1542569271/icon",
                "23,99", "88,72", "Ennuvolat"};
        items = new ArrayList<>(Arrays.asList(data));

        /*adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.
        );*/
        TextView tvCoords = view.findViewById(R.id.tvCoords);
        TextView tvWeather = view.findViewById(R.id.tvWeather);
        final EditText etCity = view.findViewById(R.id.etCity);
        Button btnRefresh = view.findViewById(R.id.btnRefresh);
        fragment = this;

        String coords = "COORDENADES";
        String wt = "WEATHER TEXT";
        String etTest = "Test City";
        tvCoords.setText(coords);
        tvWeather.setText(wt);
        etCity.setText(etTest);

        model = ViewModelProviders
                .of(this)
                .get(WeatherViewModel.class);



        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(),
                        etCity.getText().toString(),
                        Toast.LENGTH_LONG);

                toast.show();

                Log.d("HAJELKASDDA", etCity.getText().toString());

                setPreferences(getContext(), "city", etCity.getText().toString());
                model.getWeather().observe(fragment, new Observer<List<Weather>>() {
                    @Override
                    public void onChanged(@Nullable List<Weather> weathers) {
                        adapter = new WeatherAdapter(
                                getContext(),
                                0,
                                weathers
                        );
                        adapter.clear();
                        adapter.addAll(weathers);
                    }
                });
            }
        });

        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_pelis_fragment, menu);
        }


        /*etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Toast toast = Toast.makeText(getContext(),
                        s.toString(),
                        Toast.LENGTH_LONG);

                toast.show();

                setPreferences(getContext(), "city", s.toString());


                model.getWeather()
                        .observe
                                (getParentFragment()
                                        , new Observer<List<Weather>>() {
                    @Override
                    public void onChanged(@Nullable List<Weather> weathers) {
                        adapter.clear();
                        adapter.addAll(weathers);
                    }
                });

            }
        });*/
        return view;
    }


    public static void setPreferences(Context context, String key, String value) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }
}
