package com.example.marou.projecte;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class WeatherAdapter extends ArrayAdapter<Weather> {
    public WeatherAdapter(Context context, int resource, List<Weather> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Weather weather = getItem(position);
        Log.w("XXXXXXXXXXXXXX", weather.toString());

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_weathers_row, parent, false);
        }

        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvRarity = convertView.findViewById(R.id.tvRarity);
        ImageView ivImageCarta = convertView.findViewById(R.id.ivImageCarta);

        tvName.setText(weather.getName());
        tvRarity.setText(weather.getRarity());
        Glide.with(getContext()).load(weather.getImageUrl()).into(ivImageCarta);

        return convertView;

    }
}
