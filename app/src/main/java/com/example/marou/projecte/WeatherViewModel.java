package com.example.marou.projecte;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class WeatherViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final WeatherDao weatherDao;
    private MutableLiveData<Boolean> loading;

    public WeatherViewModel(Application application) {
        super(application);

        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.weatherDao = appDatabase.getWeatherDao();
        this.app = application;
    }

    public LiveData<List<Weather>> getWeather() {
        Log.d("DEBUGGGGG", "ENTRA getWeather()");
        return weatherDao.getWeather();
    }


    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if (loading == null) {
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Weather>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Weather> doInBackground(Void... voids) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            String city = preferences.getString("city", "Moscow");

            WeatherAPI api = new WeatherAPI();
            ArrayList<Weather> result;
            ArrayList<String> cities = new ArrayList<>();
            cities.add(city);

            result = api.getWeathers(cities);

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Weather> weathers) {
            super.onPostExecute(weathers);
            loading.setValue(false);
        }


    }
}
