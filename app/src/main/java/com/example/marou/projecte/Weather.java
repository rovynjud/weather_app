package com.example.marou.projecte;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Weather {
    @PrimaryKey
    private int id;
    private String imageUrl;
    private double lon;
    private double lat;
    private String weather;
    private String name;
    private String pais;

    public Weather() {
    }

    public Weather(String imageUrl, double lon, double lat, String weather, String name, String pais, int id) {
        this.imageUrl = imageUrl;
        this.lon = lon;
        this.lat = lat;
        this.weather = weather;
        this.name = name;
        this.pais = pais;
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
