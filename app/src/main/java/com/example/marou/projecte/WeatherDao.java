package com.example.marou.projecte;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WeatherDao {
    @Query("select * from weather")
    LiveData<List<Weather>> getWeather();
}
